# Instrukce k vypracování úlohy

1. Vytvořte si novou branch z větve `master` (nazvěte ji, jak chcete)
2. Do této větve můžete postupně commitovat Vaše řešení
3. Až budete s řešním spokojeni (nebo se přiblíží deadline), vytvořte z této nové branch `merge request` do `master`, a přiřaďte jako `Assignee` Nikitu
4. Termín odevzdání je **2. 5. 2021 23:59:59** - za čas odevzdání se považuje přiřazení Merge requestu cvičícímu.

---

(5b.) Vytvořte třídu hry Clicker, která by byla "komponentou" s metodou `renderTo(element)`. 

Mělo by jít nakonfigurovat:
- rozměr herního pole v px (stačí jeden rozměr, tzn width = height)
- maximální váha "kuličky" (minimální je vždy 1)
- jak často se kuličky střídají
- počet bodů, který je považován za "výhru"

Další požadavky:
- hra by se měla spouštět automaticky když je vyrenderována metodou `renderTo(element)`
- čím je větší váha kuličky, tím menší by měl být její rozměr
- na konci hry by se kuličky měly přestat objevovat a měl by se objevit text s celkovým časem hry

Příklad vytvoření a renderu hry:
```javascript
// Size: 400px x 400px
// Highest weight: 5
// Delay: 1000ms
// Victory limit: 30

const clicker = new Clicker(400, 5, 1000, 30)
clicker.renderTo(document.body)
```
[HTML + CSS markup](https://jsfiddle.net/wbxLj20g/) / [Video example](https://i.imgur.com/GTkqBMF.mp4)

(3b.) Rozšiřte třídu hry o metody:
- `reset()` - resetuje stav hry
- `pause()` - zastaví hru
- `unpause()` - pokračuje ve hře


Vytvořte ovládací panel s tlačítky pro každou metodu.

(2b.) Upravte hru tak, aby šlo při vytvoření hry zadat vlastní behavior výpisu herního stavu.